const MongoClient = require('mongodb').MongoClient

const url = 'mongodb://localhost:27017'
const dbName = 'books'

function connect (callback) {
    MongoClient.connect(url, (err, client) => {
        if (err) {
            console.error('连接出错', err)
        } else {
            const db = client.db(dbName)
            callback && callback(db) // 卧槽这个回调形式
            // client.close()
        }
    })
}

module.exports = {
    connect
}
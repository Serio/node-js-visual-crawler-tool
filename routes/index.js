var express = require('express');
const { Db } = require('mongodb');
var router = express.Router();
const model = require('../model')

/* GET home page. */
router.get('/', function(req, res, next) {
  // 第一个参数，指向views里面的index.jade文件，向它传递一个title变量
  res.render('index', { 
    title: '针对Z-Library的爬虫', 
  })
  // model.connect((db) => {
    // db.collection('users').find().toArray((err, doc) => {
    //   if (err) {
    //     console.error('获取数据出错', err)
    //   } else {
    //     res.render('index', { 
    //       title: '针对Z-Library的爬虫', 
    //     })
    //   }
    // })
  // })

});
// 注册
router.get('/regist', function (req, res, next) {
  res.render('regist',{
    title: '注册页面'
  })
})


router.get('/getBooks', function (req, res, next) {
  model.connect(async function (db) {
    let booksArr = []
    await db.collection('zbook').find().toArray((err, docs) => {
      if (err) {
        console.log('获取文章数据失败', err)
        res.send(JSON.stringify({code:"0", msg:"获取文章数据失败"}))
      } else {
        // booksArr.push(docs)
        res.send(JSON.stringify(docs))
      } 
    })
    // res.json({
    //   code: 0,
    //   data: booksArr
    // })
  })
})

module.exports = router;

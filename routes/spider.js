const express = require('express');
const router = express.Router();
const https = require('https')
const cheerio = require('cheerio')
const model = require('../model')


function filter (html, CSSSelector) {
    const $ = cheerio.load(html)
    const elems = $(CSSSelector)
    const target = []
    elems.each((index, value) => {
        let oneChild = {
            title: cheerio.load(value)('[itemprop="name"]>a').text() || '不详',
            publisher: cheerio.load(value)('[title="Publisher"]').text() || '不详',
            author: cheerio.load(value)('[itemprop="author"]').text() || '不详',
            year: cheerio.load(value)('.property_year>div.property_value').text() || '不详',
            langauge: cheerio.load(value)('.property_language>div.property_value').text() || '不详',
        }
        console.log(oneChild)
        // target.push($(value).text().replace(/\s/g, '') + ' ')
        target.push(oneChild)
    })
    return target
}

/* GET users listing. */
router.get('/', (req, res, next) => {
    console.log('进来了')
    
})
router.post('/spider', function(req, resp, next) {
    resp.on('error', (err) => {
        throw err
    })
    let data = {
        url: req.body.url,
        maxPage: req.body.maxPage,
        css: req.body.css
    }

    for(let page = data.maxPage; page; page --) {
        let fullPath = data.url + 'page=' + page
        https.get(fullPath, (res) => {
            let html = ''
            res.on('data', (chunk) => {
                html += chunk
            })
            res.on('end', () => {
            
                let targetHtml = filter (html, data.css)
                targetHtml.forEach(oneBook => {
                    console.log(oneBook)
                    model.connect((db) => {
                        db.collection('zbook').insertOne(oneBook, (err) => {
                            if  (err) {
                                console.log('插入数据失败!')
                            }
                        })
                    })
                })
            })
            
        })
    }
    resp.send(JSON.stringify({code:"1", msg:"爬取成功,请在数据库中查看"}));
});

module.exports = router;

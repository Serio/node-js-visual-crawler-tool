

## NodeJS可视化爬虫工具

### 简介

> 是为了获取大量图书的数据得到而写的爬虫练手作品。
>
> 目前界面比较简陋，主要是对Z-Library进行爬取。

### 依赖

主要是express、https、cheerio、mongodb

```json

  "scripts": {
    "start": "node ./bin/www",
    "Hstart": "nodemon ./bin/www"
  },
  "dependencies": {
    "cookie-parser": "~1.4.4",
    "debug": "~2.6.9",
    "ejs": "~2.6.1",
    "express": "~4.16.1",
    "http-errors": "~1.6.3",
    "morgan": "~1.9.1"
  },
  "devDependencies": {
    "cheerio": "^1.0.0-rc.10",
    "mongodb": "^4.5.0",
    "nodemon": "^2.0.16"
  }
```



**安装项目所需依赖**

```she
npm i
```

**快速开始**
运行项目，默认在3000端口开放，此处是使用了nodemon的热重载脚本：

```shell
npm run Hstart
```

### 实际演示

由上到下依次是：`URL`，`搜索关键词`、`最大页数`、`CSS选择器`

这里拼凑出的完整URL是: `URL`/s/`搜索关键词`/ page=`大页数`

CSS则用于选中标签

![](https://pic.rmb.bdstatic.com/bjh/8d396e1f6578da436e5c8369a9d9203a.png)

![存入到mongodb](https://pic.rmb.bdstatic.com/bjh/553de66d414c481810760fd98241f77b.png)

### 后续开发

目前只是一个demo阶段，主要作用于Z-library，后面会找时间拓展优化一下